# Agile Framework `1.0`

_Agile is a global state and logic framework for reactive Typescript & Javascript applications. Supporting frameworks like React and React Native._

#### Agile is strongly inspired by [PulseJs](https://github.com/pulse-framework/pulse)
It was mainly created to learn how Pulse works under the hood, and it was fun to 'copy' and optimize PulseJs.
I will use this project mainly for my own project because here the code style is after my taste..
So I can change things quickly and don't have to deal with a big framework which is used by many people and must function perfectly.

##### Feel free to use it but be aware that it is optimized to my needs and has no docs

<div align="center">
  <img src="https://i.pinimg.com/originals/66/70/fd/6670fd61b91760bf8f04ca0479a2e0d1.gif">
</div>
